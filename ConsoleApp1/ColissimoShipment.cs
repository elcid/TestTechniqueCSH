﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ColissimoShipment : IShipment
    {
        public int Compute(int poids, int hauteur)
        {
            return (int)(poids * 0.55 + hauteur * 0.001);
        }
    }
}
