﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Furniture
    {
        public string Name { get; set; }
        public string Desinger { get; set; }
        public double? Hauteur { get; set; }
        public double? Poids { get; set; }
        public double? Price { get; set; }

        public Furniture(string name, string designer, double? hauteur, double? poids, double? price)
        {
            Name = name;
            Desinger = designer;
            Hauteur = hauteur;
            Poids = poids;
            Price = price;
        }

        public string getName()
        {
            return this.Name;
        }

        public string getDesigner()
        {
            return this.Desinger;
        }

        public string getHauteur()
        {
            return String.Format("{0}", this.Hauteur.ToString());
        }

        public string getPoids()
        {
            return String.Format("{0}", this.Poids.ToString());
        }

        public string getPrice()
        {
            return String.Format("{0}", this.Price.ToString());
        }

        public string getFurnitureLine()
        {
           return String.Format("{0} {1} {2}cm {3}kg {4} EUR\n", getName(), getDesigner(), getHauteur(), getPoids(), getPrice());
        }

        public string getFurnitureWithShippement()
        {
            var shippement = new Shipment();
            var shippementValue = shippement.Compute((int)Poids, (int)Hauteur);
            return String.Format("{0} {1} {2}cm {3}kg {4} EUR livraison: {5}\n", getName(), getDesigner(), Hauteur, Poids, Price, shippementValue);
        }

        public string getFurnitureWithShippementColissimo()
        {
            var shippement = new ColissimoShipment();
            var shippementValue = shippement.Compute((int)Poids, (int)Hauteur);
            return String.Format("{0} {1} {2}cm {3}kg {4} EUR livraison Colissimo: {5}\n", getName(), getDesigner(), Hauteur, Poids, Price, shippementValue);
        }
    }
}
