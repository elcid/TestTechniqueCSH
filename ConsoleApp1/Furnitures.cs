﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Furnitures
    {
        private List<Furniture> collection_furnitures;
        
        public Furnitures()
        {
            collection_furnitures = new List<Furniture>();
        }

        ~Furnitures(){
            this.Serialize();
        }

        public List<Furniture> Collection_furniture { get { return collection_furnitures; } set { collection_furnitures = value; } }

        public void AddFurniture(Furniture furniture_value)
        {
            collection_furnitures.Add(furniture_value);
        }

        private void Serialize()
        {
            string json = JsonConvert.SerializeObject(this);
            StreamWriter file = new StreamWriter("output.json");
            file.Write(json);
            file.Close();
        }

        public void ParseParam(string furnitureValue)
        {
            string[] values = furnitureValue.Split(' ');
            if (values.Length != 6)
            {
                Console.WriteLine("Error: Number values Usage: \"chair Albert 200 42 150\"");
                System.Environment.Exit(1);
            }
            var temp = new Furniture(values[0], values[1], Common.ConvertToDouble(values[2].ToString()), Common.ConvertToDouble(values[3].ToString()),
                Common.ConvertToDouble(values[4]));
          
            foreach (Furniture f in collection_furnitures)
            {
              
                if (f.Name == temp.Name)
                {
                    Console.WriteLine("{0} already exist", temp.Name);
                    System.Environment.Exit(1);
                    
                    return;
                }
            }
            collection_furnitures.Add(temp);

                Serialize();            
        }

        public void Deserialize(Furnitures furnitures)
        {
            if (System.IO.File.Exists("output.json")){
                StreamReader file = new StreamReader("output.json");
                string json = file.ReadToEnd();
                file.Close();
                furnitures = JsonConvert.DeserializeObject<Furnitures>(json);
                collection_furnitures = furnitures.Collection_furniture;
           
            }   
        }

        public void PrintFurniture()
        {
            StringBuilder builtString = new StringBuilder();
            foreach (Furniture furniture in collection_furnitures)
            {
                builtString.Append(furniture.getFurnitureLine());
            }
            Console.WriteLine(builtString.ToString());
        }

        public void PrintFurnituresWithLivraison()
        {
            StringBuilder builtString = new StringBuilder();
            foreach (Furniture furniture in collection_furnitures)
            {
                builtString.Append(furniture.getFurnitureWithShippement());
            }
            Console.WriteLine(builtString.ToString());
        }

        public void PrintFurnituresWithLivraisonColissimo()
        {
            StringBuilder builtString = new StringBuilder();
            foreach (Furniture furniture in collection_furnitures)
            {
                builtString.Append(furniture.getFurnitureWithShippementColissimo());
            }
            Console.WriteLine(builtString.ToString());
        }
    }
}
