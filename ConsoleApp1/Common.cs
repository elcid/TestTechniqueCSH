﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public static class Common
    {
        public static double ConvertToDouble(string value)
        {
            if (value == null)
            {
                return 0;
            }
            else
            {
                double OutValue;
                double.TryParse(value, out OutValue);

                if (double.IsNaN(OutValue) || double.IsInfinity(OutValue))
                {
                    return 0;
                }
                return OutValue;
            }
        }
    }
}
